%define debug_package %{nil}
%define Minortag 1.0.1

Name:           libumem
Version:        1.0
Release:        4
Summary:        Port of Solaris's slab allocator.

Group:          System Environment/Libraries
License:        CDDL-1.0
URL:            https://github.com/omniti-labs/portableumem
Source0:        %{name}-%{version}.tar.gz
Patch9000:	0001-Fix-build-problem-on-linux.patch
Patch9001:	0002-correct-the-parameter-ordering-for-the-memalign-hook.patch
Patch9002:	0003-Apparently-the-glibc-malloc-hooks-are-now-deprecated.patch

BuildRequires:  autoconf >= 2.50
BuildRequires:  automake >= 1.4
BuildRequires:  libtool >= 1.4.2
BuildRequires:  doxygen
BuildRequires:  gcc
BuildRequires:  binutils
BuildRequires:  make
BuildRequires:  mktemp

ExclusiveArch: x86_64

%description
This a port of Solaris's slab allocator, libumem, to Linux.

"A slab allocator is a cache management structure for efficient use
of [...] memory. [...] It is targeted for use of many small pieces
of memory chunks. By managing small memory chunks in the units
called slabs, this mechanism enables lower fragmentation, fast allocation,
and reclaming memory." (Description sourced from Wikipedia.)

%package devel
Summary: Port of Solaris's slab allocator.
Group: Development/Libraries
Requires: pkgconfig

%description devel
This contains the libraries and header files for using this port
of Solaris's slab allocator, libumem, to Linux.

%prep
%autosetup -n %{name}-%{version}/%{Minortag} -p1

%build
./autogen.sh
%configure
%{__make} -j
%{__make} check
%{__make} html

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

# Remove the libtool files -- we don't want them.
find $RPM_BUILD_ROOT%{_libdir} -name '*.la' | xargs rm -fv

# Remove the symlink to the SONAME. Let ldconfig manage that.
rm -fv $RPM_BUILD_ROOT%{_libdir}/*.so.[0-9]

# Build the pkgconfig configurations.
mkdir -p $RPM_BUILD_ROOT%{_libdir}/pkgconfig

cat<<EOT >$RPM_BUILD_ROOT%{_libdir}/pkgconfig/%{name}-%{version}.pc
prefix=%{_prefix}
exec_prefix=%{_exec_prefix}
libdir=%{_libdir}
includedir=%{_includedir}

Name: %{name}
Version: %{version}
Description: Port of Solaris's slab allocator.
URL: https://labs.omniti.com/trac/portableumem/
Requires:
Libs: -L\${libdir} -lumem
Cflags:
EOT

%clean
rm -rf $RPM_BUILD_ROOT

%pre
/sbin/ldconfig

%post
/sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYRIGHT INSTALL NEWS OPENSOLARIS.LICENSE README
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root,-)
%doc AUTHORS COPYING COPYRIGHT INSTALL NEWS OPENSOLARIS.LICENSE README TODO
%doc docs/html
%{_includedir}/*.h
%{_includedir}/sys/*.h
%{_libdir}/*.so
%{_libdir}/*.a
%{_mandir}/man*/*
%{_libdir}/pkgconfig/*.pc

%changelog
* Thu Dec 29 2022 luocheng <luochunsheng@huawei.com> - 1.0-4
- Fix build error in glibc 2.34

* Wed Jul 20 2022 Chenyx <chenyixiong3@huawei.com> - 1.0-3
- License compliance rectification

* Mon Dec 13 2021 zhaoshuang <zhaoshuang@uniontech.com> - 1.0.1-2
- remove "%{dist}" from spec file

* Fri Nov 27 2020 luocheng <1264148725@qq.com> - 1.0.1-1
- Package init
